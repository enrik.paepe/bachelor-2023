import LoginForm from '../components/LoginForm.vue';
import { screen, userEvent } from '@storybook/testing-library';
import { linkTo } from '@storybook/addon-links';


// source: https://www.youtube.com/watch?v=yF5LShLSmb4

export default {
	title: 'Login',
	component: LoginForm
}

const Template = (args) => ({
	components: { LoginForm },
	setup() {
		return { args }
	},
	template: '<LoginForm v-bind="args"/>'
});

export const Primary = Template.bind({});

Primary.play = async () => {
	const emailInput = screen.getByLabelText('Email', {
		selector: 'input'
	});
	const passwordInput = screen.getByLabelText('Password', {
		selector: 'input'
	});
	const btn = screen.getByDisplayValue('Login', {
		selector: 'input'
	});
	await userEvent.type(emailInput, 'enrik@mail.com', {
		delay: 200
	});
	await userEvent.type(passwordInput, 'Azerty123', {
		delay: 200
	});
	userEvent.click(btn);
}

export const Secondary = Template.bind({});
Secondary.args = {
	email: 'test',
	password: 'test'
}
