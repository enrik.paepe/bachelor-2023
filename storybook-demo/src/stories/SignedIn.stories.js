import ResponsePage from '../components/ResponsePage.vue';

export default {
	title: 'ResponsePage',
	component: ResponsePage
}

const Template = (args) => ({
	components: { ResponsePage },
	setup() {
		return { args }
	},
	template: '<ResponsePage v-bind="args"/>'
});

export const Primary = Template.bind({});
Primary.args = { success: true };


export const Secondary = Template.bind({});
Secondary.args = { success: false };

