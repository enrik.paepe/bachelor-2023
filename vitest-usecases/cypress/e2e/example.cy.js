// https://on.cypress.io/api

describe('Login flow', () => {
  it('visit login page while authenticated', () => {
    localStorage.setItem('authenticated', 'true');
    cy.visit('/login')
    cy.contains('p', 'Signed in successfully')
  })
})
