import { ref, computed } from 'vue'

export function useShopHours() {
  const currentTime = ref(new Date())
  function setCurrentTime() {
    currentTime.value = new Date()
  }
  const isShopOpen = computed(() => {
    const currentHour = currentTime.value.getHours()
    return currentHour >= 9 && currentHour < 18
  })
  function getProducts() {
    return ['cheese', 'tomato', 'beef'];
  }
  return {
    currentTime,
    setCurrentTime,
    isShopOpen,
    getProducts
  }
}
