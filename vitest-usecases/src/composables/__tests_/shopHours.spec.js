import { afterEach, beforeEach, describe, expect, it, vi } from 'vitest'

import { useShopHours } from '../shopHours.js'

describe('Shop closing hours', () => {
  beforeEach(() => {
    vi.useFakeTimers();
  });

  afterEach(() => {
    vi.useRealTimers();
  });

  it('indicate shop is closed', () => {
    const date = new Date(2023, 1, 1, 8) // set time to 8 AM
    vi.setSystemTime(date);
    const isOpen = useShopHours().isShopOpen.value;
    expect(isOpen).toBe(false);
  });

  it('allows purchases within business hours', () => {
    const date = new Date(2023, 1, 1, 15)
    vi.setSystemTime(date)
    const isOpen = useShopHours().isShopOpen.value
    expect(isOpen).toBe(true)
  })

  it('test if sold products changed', () => {
    const shop = useShopHours();
    expect(shop.getProducts()).toMatchSnapshot();
  });
})

