import { mount } from '@vue/test-utils'
import { beforeEach, describe, expect, it } from 'vitest'
import LoginPage from '../LoginPage.vue'
import { createPinia, setActivePinia } from "pinia";
import { useAuthStore } from "@/stores/auth";


beforeEach(() => {
  setActivePinia(createPinia()) // provided by Pinia
});

describe('Login flow', () => {
  it('sign in with correct credentials', async () => {
    const auth = useAuthStore()
    const loginData = {
      email: 'enrik@gmail.com',
      password: 'Azerty123'
    }
    const wrapper = await setLoginValues(loginData);
    const submit = wrapper.find('input[type=submit]')

    await submit.trigger('click')
    await delay(0.1);
    expect(auth.getName).toBe('enrik'); // success
    expect(wrapper.text()).toContain('Signed in successfully')
  })

  it('sign in with incorrect credentials', async () => {
    const loginData = {
      email: 'wrong-user@gmail.com',
      password: 'Azerty123'
    }
    const wrapper = await setLoginValues(loginData);
    const submit = wrapper.find('input[type=submit]')
    await submit.trigger('click')
    await delay(0.1)
    expect(wrapper.text()).toContain('Incorrect credentials')
  })
})

function delay(seconds) {
  return new Promise((resolve) => {
    setTimeout(resolve, seconds * 1000)
  })
}

async function setLoginValues(loginData) {
  const wrapper = mount(LoginPage)
  const email = wrapper.find('input[type=text]')
  const password = wrapper.find('input[type=password]')
  await email.setValue(loginData.email)
  await password.setValue(loginData.password)
  return wrapper;
}


export function useShopHours() {

  const currentTime = ref(new Date());

  function setCurrentTime() {
    currentTime.value = new Date()
  }

  const isShopOpen = computed(() => {
    const currentHour = currentTime.value.getHours()
    return currentHour >= 9 && currentHour < 18
  });

  return {
    currentTime,
    setCurrentTime,
    isShopOpen
  }
}


