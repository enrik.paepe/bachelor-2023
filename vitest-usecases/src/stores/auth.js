import { defineStore } from 'pinia';

export const useAuthStore = defineStore('auth', {
  state: () => ({
    user: {
      email: ''
    }
  }),
  getters: {
    getName(state) {
      return state.user?.email?.split('@')[0] ?? '';
    },
  },
  actions: {
    setUser(email) {
      this.user = { email: email}
    }
  }
});
