import { setActivePinia, createPinia } from 'pinia'
import { useAuthStore } from '../auth.js'
import { beforeEach, describe, expect, it } from 'vitest'


describe('Counter Store', () => {
  beforeEach(() => {
    setActivePinia(createPinia())
  })

  it('returns correct user', () => {
    const auth = useAuthStore()
    auth.setUser('enrik@gmail.com');
    expect(auth.getName).toBe('enrik');
  });
});
