import { afterAll, afterEach, beforeAll } from 'vitest'
import { setupServer } from 'msw/node'
import { rest } from 'msw'

const userResponseSuccess = {
  message: 'Success',
  name: 'Enrik'
}

const userResponseFailure = {
  message: 'Failure'
}

const credentials = {
  email: 'enrik@gmail.com',
  password: 'Azerty123'
}

export const restHandlers = [
  rest.post('https://testpage/login', async (req, res, ctx) => {
    const body = (await req.json()).credentials
    const email = body.email._value;
    const password = body.password._value;
    if (email === credentials.email && password === credentials.password)
      return res(ctx.status(200), ctx.json(userResponseSuccess))
    else {
      return res(ctx.status(401), ctx.json(userResponseFailure))
    }
  })
]

const server = setupServer(...restHandlers)

beforeAll(() => server.listen({ onUnhandledRequest: 'error' }))

afterAll(() => server.close())

afterEach(() => server.resetHandlers())
