const fs = require('fs');
const path = require('path');

const fileCount = (process.argv[2] ?? 0) / 2;

let motherComponent = '<template>';

const motherComponentImports = [];

if(!fs.existsSync('organisms')) fs.mkdirSync('./src/organisms');
if(!fs.existsSync('molecules')) fs.mkdirSync('./src/molecules');
if(!fs.existsSync('atoms')) fs.mkdirSync('./src/atoms');


for (let i = 0; i < fileCount; i++) {
	const moleculePath = path.join('./src/molecules', `molecule${i}.vue`);
	const atomPath = path.join('./src/atoms', `atom${i}.vue`);
	fs.writeFileSync(moleculePath,generateMolecule(generateRandomString(i), i));
	fs.writeFileSync(atomPath,generateAtom(generateRandomString(i)));
	motherComponentImports.push(`import Molecule${i} from './molecules/molecule${i}.vue';`);
	motherComponent+=`<Molecule${i}></Molecule${i}>\n`
};

motherComponent+='</template>\n'
motherComponent+='<script setup>\n'
motherComponent+= motherComponentImports.join('\n');
motherComponent+='</script>\n'



fs.writeFileSync(path.join('./src', `app.vue`), motherComponent);


function generateAtom(randomContent) {
	return `
<template>
	<p>Vue atom {{ id }}</p>
	<div class="container-style">
		<p>This is the atom component</p>
		<li> Random content: ${randomContent} </li>
	</div>
</template>

<script setup>
	defineProps({
		id: {
			type: Number,
			default: 0
		}
	});
</script>
<style scoped>
	.container-style {
		background:lightblue;
	}
</style>
	`
}

function generateMolecule(randomContent, id) {
	return `
	<template>
	<p>Vue molecule {{ id }}</p>
	<div class="container-style">
		<p>This is the molecule component</p>
		<Atom${id} :id=${id}></Atom${id}>
		${randomContent}
	</div>
</template>

<script setup>
	import Atom${id} from '../atoms/atom${id}.vue';

	defineProps({
		id: {
			type: Number,
			default: 0
		}
	});
</script>
<style scoped>
	.container-style {
		background:green;
	}
</style>
	`
}

function generateRandomString(length) {
	let result = '';
	const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  
	for (let i = 0; i < length; i++) {
	  const randomIndex = Math.floor(Math.random() * characters.length);
	  result += characters.charAt(randomIndex);
	}
	return result;
  }